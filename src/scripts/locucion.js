// AUDIO LOCUCION
var intro = [];
intro.push(new Howl({ src: require('../audio/locucion/intro1.mp3') }));
intro.push(new Howl({ src: require('../audio/locucion/intro2.mp3') }));
intro.push(new Howl({ src: require('../audio/locucion/intro3.mp3') }));
intro.push(new Howl({ src: require('../audio/locucion/intro4.mp3') }));

var tema1 = [];
tema1.push(new Howl({ src: require('../audio/locucion/a1_0.mp3') }));
tema1.push(new Howl({ src: require('../audio/locucion/a1_1a.mp3') }));
tema1.push(new Howl({ src: require('../audio/locucion/a1_1.mp3') }));
tema1.push(new Howl({ src: require('../audio/locucion/a1_2.mp3') }));
tema1.push(new Howl({ src: require('../audio/locucion/a1_3.mp3') }));
tema1.push(new Howl({ src: require('../audio/locucion/a1_4.mp3') }));
tema1.push(new Howl({ src: require('../audio/locucion/a1_5.mp3') }));
tema1.push(new Howl({ src: require('../audio/locucion/a1_6.mp3') }));
tema1.push(new Howl({ src: require('../audio/locucion/a1_7.mp3') }));
tema1.push(new Howl({ src: require('../audio/locucion/a1_8.mp3') }));

var tema2 = [];
tema2.push(new Howl({ src: require('../audio/locucion/a2_0.mp3') }));
tema2.push(new Howl({ src: require('../audio/locucion/a2_1.mp3') }));
tema2.push(new Howl({ src: require('../audio/locucion/a2_1_1.mp3') }));
tema2.push(new Howl({ src: require('../audio/locucion/a2_1_2.mp3') }));
tema2.push(new Howl({ src: require('../audio/locucion/a2_1_3.mp3') }));
tema2.push(new Howl({ src: require('../audio/locucion/a2_2.mp3') }));
tema2.push(new Howl({ src: require('../audio/locucion/a2_3.mp3') }));
tema2.push(new Howl({ src: require('../audio/locucion/a2_4.mp3') }));

var tema3 = [];
tema3.push(new Howl({ src: require('../audio/locucion/a3_0.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_1.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_1_1.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_1_2.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_2.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_3.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_4.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_5.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_5_1.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_5_2.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_5_3.mp3') }));
tema3.push(new Howl({ src: require('../audio/locucion/a3_5_4.mp3') }));

// correcciones |1|
tema4 = [];
tema4.push(new Howl({ src: require('../audio/locucion/a4_0.mp3') }));
tema4.push(new Howl({ src: require('../audio/locucion/a4_1_1.mp3') }));
tema4.push(new Howl({ src: require('../audio/locucion/a4_1_2.mp3') }));
tema4.push(new Howl({ src: require('../audio/locucion/a4_1_3.mp3') }));
tema4.push(new Howl({ src: require('../audio/locucion/a4_1_4.mp3') }));
tema4.push(new Howl({ src: require('../audio/locucion/a4_1_5.mp3') }));

tema5 = [];
tema5.push(new Howl({ src: require('../audio/locucion/a5_0.mp3') }));
tema5.push(new Howl({ src: require('../audio/locucion/a5_1.mp3') }));
tema5.push(new Howl({ src: require('../audio/locucion/a5_2.mp3') }));

tema6 = [];
tema6.push(new Howl({ src: require('../audio/locucion/a6_0.mp3') }));
tema6.push(new Howl({ src: require('../audio/locucion/a6_1.mp3') }));

final = [];
final.push(new Howl({ src: require('../audio/locucion/final.mp3') }));

export function locStop(params) {
	intro.forEach((loc) => {
		loc.stop();
	});
	tema1.forEach((loc) => {
		loc.stop();
	});
	tema2.forEach((loc) => {
		loc.stop();
	});
	tema3.forEach((loc) => {
		loc.stop();
	});
	tema4.forEach((loc) => {
		loc.stop();
	});
	tema5.forEach((loc) => {
		loc.stop();
	});
	tema6.forEach((loc) => {
		loc.stop();
	});
	final.forEach((loc) => {
		loc.stop();
	});
}

export var intro;
export var tema1;
export var tema2;
export var tema3;
export var tema4;
export var tema5;
export var tema6;
export var final;
