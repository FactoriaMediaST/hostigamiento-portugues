import '../styles/index.scss';
import Reveal from '../vendor/reveal/js/reveal';
import QueryLoader2 from 'queryloader2';
import anime from 'animejs/lib/anime.es.js';
import spine from './render';
import toastr from 'toastr';
import { Howl, Howler } from 'howler';
import {
	intro,
	tema1,
	tema2,
	tema3,
	tema4,
	tema5,
	tema6,
	final,
	locStop,
} from './locucion';
import interact from 'interactjs';
// import "./app";

var musica_fondo = new Howl({
	src: require('../audio/background.mp3'),
	volume: 0.1,
	loop: true,
});
var actividad = new Howl({
	src: require('../audio/actividad.mp3'),
	volume: 0.5,
	loop: true,
});
var win = new Howl({ src: require('../audio/win.ogg'), volume: 0.3 });
var botoAu = new Howl({ src: require('../audio/next.mp3'), volume: 0.5 });
var acierto = new Howl({ src: require('../audio/win.mp3'), volume: 0.5 });
var error = new Howl({ src: require('../audio/error.mp3'), volume: 0.5 });
var particula = document.querySelector('#particles-js');
var puntaje = 0;
var SD = window.parent;
var puntSave;

toastr.options = {
	closeButton: false,
	debug: false,
	newestOnTop: false,
	progressBar: false,
	positionClass: 'toast-bottom-center',
	preventDuplicates: true,
	onclick: null,
	showDuration: '300',
	hideDuration: '1000',
	timeOut: '5000',
	extendedTimeOut: '300',
	showEasing: 'swing',
	hideEasing: 'linear',
	showMethod: 'fadeIn',
	hideMethod: 'fadeOut',
};
// SETUP

QueryLoader2(document.querySelector('body'), {
	barColor: '#ffffff',
	backgroundColor: '#d10122',
	percentage: true,
	barHeight: 1,
	minimumTime: 200,
	fadeOutTime: 1000,
	onComplete: () => {
		anime({
			targets: '#carcasa',
			opacity: 1,
			duration: 500,
		});
		window.Reveal = Reveal;

		Reveal.initialize({
			controls: false,
			progress: false,
			history: false,
			center: false,
			slideNumber: false,
			keyboard: false,
			hash: false,
			viewDistance: 4,
			preloadIframes: false,
			transition: 'slide', //none/fade/slide/convex/concave/zoom
			width: 1920,
			height: 1080,
			dependencies: [
				{
					src: require('../vendor/plugin/transit/transit'),
					async: false,
				},
				{
					src: require('../vendor/plugin/appearance/appearance'),
					async: false,
				},
			],
		});

		var passAud = true,
			passAud1 = true,
			passAud2 = true,
			passAud3 = true,
			passAud4 = true,
			passAud5 = true,
			passAud6 = true;

		var cont1 = 0;
		var cont3 = 0;
		var cont4 = 0;

		musica_fondo.play();
		var boton_mute = document.getElementById("boton_sonido");

		function stopBack() {
			musica_fondo.pause();
			actividad.pause();
		}

		let canvasPool = [];
		for (let i = 0; i < 4; i++) {
			canvasPool.push(document.createElement('canvas'));
		}

		if (!SD.GetBookmark) {
			SD.GetBookmark = function () {
				return localStorage.getItem('bookmark')
					? localStorage.getItem('bookmark')
					: '';
			};
			SD.SetBookmark = function (bookmark) {
				localStorage.setItem('bookmark', bookmark);
			};
			SD.SetScore = function () {
				return false;
			};
			SD.GetDataChunk = function () {
				return localStorage.getItem('data');
			};
			SD.SetDataChunk = function (data) {
				localStorage.setItem('data', data);
			};
			SD.ResetStatus = function () {
				return null;
			};
		}

		Reveal.addEventListener('ready', (event) => {
			// console.log(event.indexh, event.indexv);
			if (event.indexh == 0) {
				intro[0].play();
			}
		});

		var siguiente_sonido = new Audio(
			require('../audio/next.ogg'),
			require('../audio/next.mp3')
		);
		var bookMK;
		var puntoGuard;
		// siguiente_sonido.play();
		var activeWidgets = [];
		Reveal.addEventListener('slidechanged', function (event) {
			bookMK = SD.GetBookmark();
			puntoGuard = SD.GetDataChunk();
			// event.previousSlide, event.currentSlide, event.indexh, event.indexv
			if (event.indexh > 3 && event.indexh < 23) {
				SD.SetBookmark(event.indexh);
			}
			if (event.indexh > 28) {
				if (puntoGuard == 0) {
					Reveal.slide(22);
				} else {
					SD.SetBookmark(event.indexh);
				}
			}
			// console.log(puntaje);
			// console.log(SD.GetBookmark());
			locStop();
			if (event.previousSlide) {
				var canvas = event.previousSlide.querySelectorAll('canvas');
				for (let i = 0; i < canvas.length; i++) {
					const canvae = canvas[i];
					canvasPool.push(canvae);
				}
				activeWidgets.forEach((widget, index) => {
					if (widget.stop) {
						widget.stop();
						activeWidgets.splice(index, 1);
					}
				});
			}
			// let slideId = event.currentSlide.classList[0];
			// let iframe = document.querySelector(`.slide-background.${slideId} .slide-background-content iframe`);

			switch (event.indexh) {
				case 0:
					intro[0].play();
					break;
				case 1:
					if (bookMK < 2) {
						Reveal.downTo(0);
						intro[1].play();
					} else {
						Reveal.downTo(2);
					}
					break;
				case 2:
					intro[2].play();
					break;
				case 3:
					intro[3].play();
					break;
				case 4:
					tema1[0].play();
					break;
				case 5:
					switch (event.indexv) {
						case 0:
							if (passAud1) {
								passAud1 = false;
								tema1[1].play();
							}
							break;
						case 1:
							tema1[3].play();
							tema1[3].on('end', function () {
								setTimeout(() => {
									Reveal.downTo(0);
									cont1++;
									if (cont1 >= 2) {
										activ_btn('.PG_6', '.next');
									} else {
										activ_btn('.PG_6', '.icon_1');
									}
								}, 300);
							});
							break;
						case 2:
							tema1[2].play();
							tema1[2].on('end', function () {
								setTimeout(() => {
									Reveal.downTo(0);
									cont1++;
									if (cont1 >= 2) {
										activ_btn('.PG_6', '.next');
									} else {
										activ_btn('.PG_6', '.icon_2');
									}
								}, 300);
							});
							break;
					}
					break;
				case 6:
					tema1[4].play();
					tema1[4].on('end', function () {
						setTimeout(() => {
							Reveal.next();
						}, 500);
					});
					break;
				// case 7:
				// 	// tema1[9].play();
				// 	break;
				case 8:
					tema1[8].play();
					break;
				case 9:
					tema1[9].play();
					break;
				case 10:
					tema2[0].play();
					break;
				case 11:
					switch (event.indexv) {
						case 0:
							if (passAud2) {
								passAud2 = false;
								tema2[1].play();
							}
							break;
						case 1:
							tema2[2].play();
							tema2[2].on('end', function () {
								setTimeout(() => {
									Reveal.downTo(0);
									activ_btn('.PG_12', '.icon_2');
									cont3++;
									if (cont3 >= 3) {
										document.getElementById('btn_pg_12').classList.remove('no-view');
									}
								}, 400);
							});
							break;
						case 2:
							tema2[3].play();
							tema2[3].on('end', function () {
								setTimeout(() => {
									Reveal.downTo(0);
									activ_btn('.PG_12', '.icon_3');
									cont3++;
									if (cont3 >= 3) {
										document.getElementById('btn_pg_12').classList.remove('no-view');
									}
								}, 400);
							});
							break;
						case 3:
							tema2[4].play();
							tema2[4].on('end', function () {
								setTimeout(() => {
									Reveal.downTo(0);
									activ_btn('.PG_12', '.next');
									cont3++;
									if (cont3 >= 3) {
										document.getElementById('btn_pg_12').classList.remove('no-view');
									}
								}, 400);
							});
							break;
					}
					break;
				case 12:
					break;
				case 13:
					tema2[5].play();
					tema2[5].on('end', function () {
						setTimeout(() => {
							Reveal.next();
						}, 600);
					});
					break;
				case 14:
					tema2[6].play();
					break;
				case 15:
					tema2[7].play();
					break;
				case 16:
					tema3[0].play();
					break;
				case 17:
					switch (event.indexv) {
						case 0:
							if (passAud3) {
								passAud3 = false;
								tema3[1].play();
							}
							break;
						case 1:
							tema3[2].play();
							tema3[2].on('end', function () {
								setTimeout(() => {
									Reveal.downTo(0);
									cont4++;
									if (cont4 >= 2) {
										activ_btn('.PG_18', '.next');
									} else {
										activ_btn('.PG_18', '.icon_2');
									}
								}, 500);
							});
							break;
						case 2:
							tema3[3].play();
							tema3[3].on('end', function () {
								setTimeout(() => {
									Reveal.downTo(0);
									cont4++;
									if (cont4 >= 2) {
										activ_btn('.PG_18', '.next');
									} else {
										activ_btn('.PG_18', '.icon_1');
									}
								}, 500);
							});
							break;
					}
					break;

				case 18:
					tema3[4].play();
					break;
				case 19:
					tema3[5].play();
					break;
				case 20:
					tema3[6].play();
					break;
				case 21:
					tema3[7].play();
					break;
				case 22:
					boton_mute.classList.add("invisible");
					locStop();
					stopBack();
					actividad.play();
					break;
				case 23:
					switch (event.indexv) {
						case 0:
							Reveal.downTo(0);
							// tema1[33].play();
							// tema1[33].on('end', function () {
							// 	Reveal.downTo(1);
							// });
							break;
					}
					break;
				// case 24:
				// switch (event.indexv) {
				// 	case 0:
				// 		break;
				// }
				// break;
				case 28:
					stopBack();
					musica_fondo.play();
					if (puntaje > 50) {
						Reveal.downTo(0);
					} else {
						Reveal.downTo(1);
					}
					SD.SetDataChunk(puntaje);
					break;
				case 29:
					boton_mute.classList.remove("invisible");
					if (puntaje > 25) {
						tema4[0].play();
					}
					break;
				case 30:
					switch (event.indexv) {
						case 0:
							tema4[1].play();
							// if (passAud4) {
							// 	passAud4 = false;
							// 	tema4[1].play();
							// 	tema4[1].on('end', function () {
							// 		tema4[2].play();
							// 	});
							// } else {
							// 	tema4[2].play();
							// }
							break;
						case 1:
							tema4[2].play();
							break;
						case 2:
							tema4[3].play();
							break;
						case 3:
							tema4[4].play();
							break;
						case 4:
							tema4[5].play();
							break;
					}
					break;

				case 31:
					tema5[0].play();
					break;
				case 32:
					tema5[1].play();
					break;
				// case 33:
				// 	// win.play();
				// 	break;
				case 34:
					tema5[2].play();
					break;
				case 39:
					tema6[0].play();
					break;

				case 40:
					tema6[1].play();
					break;
				case 41:
					final[0].play();
					break;
			}

			var widgets = event.currentSlide.querySelectorAll('.lazy-widget');

			for (let i = 0; i < widgets.length; i++) {
				const widget = widgets[i];
				let canvas = canvasPool.pop();
				widget.append(canvas);
				let style = window.getComputedStyle(widget);

				canvas.width = style.width.replace('px', '');
				canvas.height = style.height.replace('px', '');

				var spineWidget = new spine(
					canvas,
					widget.dataset.json,
					widget.dataset.atlas,
					widget.style.width,
					widget.style.height,
					widget.dataset.animation
				);
				spineWidget.create();
				activeWidgets.push(spineWidget);
				// setTimeout(() => {
				// playHablando();
				// }, 1000);

				function playHablando() {
					if (spineWidget.assetManager.isLoadingComplete()) {
						let playing = tema1.find((locucion) => locucion.playing());
						if (playing && i == 0) {
							// spineWidget.state.setAnimation(2, "hablando", true);
							// console.log(playing.duration() * 1000);
							setTimeout(() => {
								spineWidget.state.clearTrack(2);
							}, playing.duration() * 1000);
						}
					} else {
						requestAnimationFrame(() => {
							playHablando();
						});
					}
				}
			}
		});

		initAudios();
	},
});

// preload assets
require.context('../assets');
var widgets = document.querySelectorAll('.lazy-widget');
for (let i = 0; i < widgets.length; i++) {
	const widget = widgets[i];
	let image = new Image();
	image.src = widget.dataset.json.replace('json', 'png');
}
var pausa = false;
window.sonido = (button) => {
	button.classList.remove('off');
	button.classList.remove('on');
	if (!pausa) {
		musica_fondo.pause();
		pausa = true;
		button.classList.add('off');
	} else {
		musica_fondo.play();
		pausa = false;
		button.classList.add('on');
	}
};

window.menuButton = (button, slide) => {
	button.classList.add('baw');
	Reveal.slide(slide);
};
window.retornar = () => {
	Reveal.downTo(0);
	setTimeout(() => {
		Reveal.right();
	}, 200);
};

window.respuesta = (punt, slide) => {
	Reveal.downTo(slide);
	var GTPT = SD.GetDataChunk();
	puntaje = Number(punt) + Number(GTPT);
	SD.SetDataChunk(puntaje);
	if (punt == 25) {
		acierto.play();
	} else {
		error.play();
	}
};

window.mapa = function () {
	window.parent.postMessage({ type: 'puntaje', puntaje: 100 }, '*');
	window.parent.postMessage({ type: 'mapa' }, '*');
};

window.goToGame = () => {
	window.location.href = window.location.href.replace('teoria', 'reto');
};

window.activ_btn = function (val, icon) {
	Reveal.downTo(0);
	let drivBtn = document.querySelectorAll(val + ' .btn_driv');
	let next = document.querySelector(val + ' .next');
	let back = document.querySelector(val + ' .back1');
	drivBtn.forEach((element) => {
		element.classList.remove('infinite');
		element.classList.remove('pulse');
		element.classList.add('fadeInUp');
	});
	let iconoAnim = document.querySelector(val + ' ' + icon);
	iconoAnim.addEventListener('webkitAnimationEnd', myEndFunction);
	function myEndFunction() {
		iconoAnim.classList.remove('fadeInUp');
		iconoAnim.classList.add('pulse');
		iconoAnim.classList.add('infinite');
		iconoAnim.removeEventListener('webkitAnimationEnd', myEndFunction);
	}
	if (icon == '.next') {
		next.classList.remove('blockeado');
		// back.classList.remove("blockeado");
	}
};
var cont2 = 0;
window.showMsg = function (val, my) {
	cont2++;
	let btn = document.querySelector('.m_' + val);
	btn.classList.remove('no-view');
	btn.classList.add('animated');
	btn.classList.add('rollIn');
	my.style.display = 'none';
	locStop();
	switch (val) {
		case 1:
			tema1[5].play();
			tema1[5].on('end', function () {
				verifCont2();
			});
			break;
		case 2:
			tema1[6].play();
			tema1[6].on('end', function () {
				verifCont2();
			});
			break;
		case 3:
			tema1[7].play();
			tema1[7].on('end', function () {
				verifCont2();
			});
			break;
	}
};
function verifCont2() {
	if (cont2 >= 3) {
		document.getElementById('pg8_id').classList.remove('no-view');
	}
}

var puntos = 0;
window.showAnsw = function (val, my) {
	if (my) {
		acierto.play();
		let btn = document.querySelectorAll('.btnP' + val);
		let answ = document.querySelector('.btnPA' + val);
		btn.forEach((element) => {
			element.classList.add('blockeado');
		});
		answ.classList.remove('no-view');
		puntos++;
	} else {
		error.play();
	}
	if (puntos >= 3) {
		let cont = document.querySelector('.espec');
		cont.classList.remove('no-view');
	}
};
var caspt = 0;
window.botOP = function (val) {
	let btn = document.querySelector('.pat_' + val);
	btn.classList.remove('no-view');
	locStop();
	switch (val) {
		case 1:
			tema3[8].play();
			caspt++;
			break;
		case 2:
			tema3[9].play();
			caspt++;
			break;
		case 3:
			tema3[10].play();
			caspt++;
			break;
		case 4:
			tema3[11].play();
			caspt++;
			break;
	}
	if (caspt >= 4) {
		let cont = document.querySelector('.espec1');
		cont.classList.remove('no-view');
	}
};
var opcDrag = 0;
window.nexACB = function (val) {
	let btn = document.querySelector('.dropA_' + val);
	// locStop();
	if (val == 2) {
		acierto.play();
	} else {
		error.play();
	}
};

var next = document.getElementsByClassName('next');
for (let i = 0; i < next.length; i++) {
	const element = next[i];
	element.onclick = () => {
		let playing = locucion.find((locucion) => !locucion.paused);
		if (playing) {
			toastr.options.timeOut = (playing.duration - playing.currentTime) * 1000;
			return toastr['info'](
				'No puedes avanzar mientras la locución se esta reproduciendo'
			);
		}
		Reveal.right();
	};
}

var back = document.getElementsByClassName('back');
for (let i = 0; i < back.length; i++) {
	const element = back[i];
	element.onclick = () => Reveal.prev();
}

var menuBotones = document.getElementsByClassName('menu-btn');
for (let i = 0; i < menuBotones.length; i++) {
	const element = menuBotones[i];
	element.onclick = () => {
		let playing = locucion.find((locucion) => !locucion.paused);
		if (playing) {
			toastr.options.timeOut = (playing.duration - playing.currentTime) * 1000;
			return toastr['info'](
				'No puedes avanzar mientras la locución se esta reproduciendo'
			);
		}

		if (element.dataset.gray) element.classList.add('baw');

		Reveal.slide(element.dataset.menu);
	};
}

window.endGame = function () {
	var puntFN = SD.GetDataChunk();
	// console.log(puntFN);
	SD.SetScore(puntFN, 100, 0);
	SD.SetPassed();
	SD.SetReachedEnd();
	SD.Finish();
	SD.ConcedeControl();
};

Reveal.downTo = function (to) {
	let indexh = Reveal.getIndices().h;
	Reveal.slide(indexh, to);
};

Reveal.doubleBack = function (to) {
	let twice = Reveal.getIndices().h;
	Reveal.slide(twice - 2);
};

Reveal.leftZero = function (to) {
	Reveal.left();
	let indexh = Reveal.getIndices().h;
	Reveal.slide(indexh, 0);
};

window.addEventListener('message', (event) => {
	if (event.data.message == 'end') {
		Reveal.next();
	}
});

function removeFrame() {
	var iframes = document.getElementsByTagName('iframe');
	for (var i = 0; i < iframes.length; i++) {
		iframes[i].parentNode.removeChild(iframes[i]);
	}
}

let locucion = [];

function initAudios() {
	Reveal.addEventListener('slidechanged', function (event) {
		// event.previousSlide, event.currentSlide, event.indexh, event.indexv
		locucion.forEach((locucion) => {
			locucion.pause();
			locucion.currentTime = 0;
		});
	});
}

var buttons = document.getElementsByClassName('buttons');
for (let i = 0; i < buttons.length; i++) {
	const element = buttons[i];
	const buttonImages = element.querySelectorAll('img');
	for (let i = 0; i < buttonImages.length; i++) {
		const button = buttonImages[i];
		button.onclick = (e) => {
			e.stopPropagation();
			let target = document.getElementById(button.dataset.target);

			for (let i = 0; i < buttonImages.length; i++) {
				const otherButton = buttonImages[i];
				otherButton.src = otherButton.src.replace(
					otherButton.src.substring(otherButton.src.lastIndexOf('/') + 1),
					'unselected.png'
				);
			}
			button.src = button.src.replace(
				button.src.substring(button.src.lastIndexOf('/') + 1),
				'selected.png'
			);

			target.src = target.src.replace(
				target.src.substring(target.src.lastIndexOf('/') + 1),
				button.dataset.image
			);
		};
	}
}

// function resumeGame() {

//     var bookmark = SD.GetBookmark();

//     switch (bookmark) {
//         case "1":
//             Game.guia.destino = locacionPasos[0];
//             Game.guia.state = characterStates.MOVING;
//             createEntradaA(Game);
//             break;
//         case "2":
//             Game.guia.destino = locacionPasos[1];
//             Game.guia.state = characterStates.MOVING;
//             createEntradaB(Game);
//             break;
//         case "3":
//             Game.guia.destino = locacionPasos[2];
//             Game.guia.state = characterStates.MOVING;
//             createEntradaC(Game);
//             break;
//         case "4":
//             Game.guia.destino = locacionPasos[3];
//             Game.guia.state = characterStates.MOVING;
//             createEntradaD(Game);
//             break;
//         case "5":
//             createMaster_1(Game);
//             createMaster_2(Game);
//             createMaster_3(Game);
//             createMaster_4(Game);
//             break;
//     }
// }

// function validBookMark(params) {
//   var bookmark = SD.GetBookmark();
//   if (bookmark != "") {
//       Reveal.slide(1);
//   } else {
//       SD.SetBookmark("1");
//       Reveal.slide(2);
//   }
// }

// function endGame() {
//     SD.SetScore(100, 100, 0);
//     SD.SetBookmark('end')
//     SD.SetPassed();
//     SD.SetReachedEnd();
//     SD.Finish();
//     SD.ConcedeControl();
// }
// SD.SetBookmark("4");
// if (!SD.GetBookmark) {
//     SD.GetBookmark = function () { return localStorage.getItem('bookmark') ? localStorage.getItem('bookmark') : "" };
//     SD.SetBookmark = function (bookmark) { localStorage.setItem('bookmark', bookmark) };
//     SD.SetScore = function () { return false };
//     SD.GetDataChunk = function () {
//         return localStorage.getItem('data');
//     };
//     SD.SetDataChunk = function (data) {
//         localStorage.setItem('data', data)
//     };
//     SD.ResetStatus = function () { return null };
// }
window.cargarPartida = function () {
	var bookmark = SD.GetBookmark();
	Reveal.slide(bookmark);
	puntSave = SD.GetDataChunk();
	puntaje = puntSave;
};

window.nuevaPartida = function () {
	puntaje = 0;
	SD.SetScore(puntaje, 100, 0);
	SD.ResetStatus();
	SD.SetDataChunk(puntaje);
	SD.SetBookmark('0');
	Reveal.downTo(0);
};
